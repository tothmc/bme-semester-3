-- general settings
-- ================
-- do not echo
set echo off
-- do not print substitution before/after text
set verify off
-- set date format
alter session set NLS_DATE_FORMAT='YYYY-MM-DD';
-- allow PL/SQL output
set serveroutput on
-- disable feedback, eg. anonymous block completed
set feedback off

-- t�blaeldob�sok ide
DROP SEQUENCE menetrendid_seq;
DROP TABLE menetrend;
DROP SEQUENCE repulogepid_seq;
DROP TABLE repulogep;
DROP SEQUENCE jaratid_seq;
DROP TABLE jarat;

prompt <tasks>

prompt <task n="1.1">
prompt <![CDATA[

-- 1.1-es feladat megold�sa ide

prompt ]]>
prompt </task>

prompt <task n="1.2">
prompt <![CDATA[

-- 1.2-es feladat megold�sa ide
CREATE SEQUENCE repulogepid_seq;
CREATE TABLE repulogep(
    id INTEGER DEFAULT repulogepid_seq.nextval NOT NULL,
    lajstromjel VARCHAR(7) CHECK (REGEXP_LIKE(lajstromjel, '[A-Z0-9]')) NOT NULL,
    tipusjelzes VARCHAR(7) CHECK (REGEXP_LIKE(tipusjelzes, '[A-Z0-9]')) NOT NULL,
    utolso_muszaki DATE CHECK (utolso_muszaki > DATE'2005-01-01') NOT NULL,
    meghajtas VARCHAR(11) DEFAULT 'LEGCSAVAROS' NOT NULL, 
    utolso_nagyjavitas DATE NOT NULL, 
    max_hatotav NUMBER CHECK (max_hatotav BETWEEN 300 AND 20000) NOT NULL,
    ulesek_szama INTEGER DEFAULT 320 CHECK(ulesek_szama BETWEEN 70 AND 856),
    megjegyzes LONG, 
    
    CONSTRAINT repulogep_pk PRIMARY KEY (id),
    CONSTRAINT meghajtas_chk  CHECK (meghajtas IN ('SUGARHAJTAS', 'LEGCSAVAROS')),
    CONSTRAINT max_hatotav_chk CHECK (meghajtas NOT LIKE 'LEGCSAVAROS' OR max_hatotav <= 2000)
);

CREATE SEQUENCE jaratid_seq;
CREATE TABLE jarat(
    id INTEGER DEFAULT jaratid_seq.nextval NOT NULL,
    jaratszam VARCHAR(6) CHECK (REGEXP_LIKE(jaratszam, '[A-Z0-9]')) NOT NULL,
    ind_hely VARCHAR(3) CHECK (REGEXP_LIKE(ind_hely, '[A-Z]')) NOT NULL,
    erk_hely VARCHAR(3) CHECK (REGEXP_LIKE(erk_hely, '[A-Z]')) NOT NULL,
    ind_ido DATE NOT NULL,
    erk_ido DATE NOT NULL, 
    
    CONSTRAINT jarat_pk PRIMARY KEY (id),
    CONSTRAINT jarat_jaratszam UNIQUE (jaratszam),
    CONSTRAINT erkezes_kesobb_mint_indulas CHECK (erk_ido > ind_ido)
);

CREATE SEQUENCE menetrendid_seq;
CREATE TABLE menetrend(
    id INTEGER DEFAULT menetrendid_seq.nextval NOT NULL,
    repulogepid INTEGER NOT NULL,
    jaratid INTEGER NOT NULL,
    repulesi_napok NUMBER(3) DEFAULT 0 CHECK (repulesi_napok BETWEEN 0 AND 127) NOT NULL,
    pilota_adatai LONG,
    
    CONSTRAINT menetrend_pk PRIMARY KEY (id),
    CONSTRAINT menetrend_fk_repulogepid FOREIGN KEY(repulogepid) REFERENCES repulogep(id),
    CONSTRAINT menetrend_fk_jaratid FOREIGN KEY(jaratid) REFERENCES jarat(id)
);

prompt ]]>
prompt </task>

-- �gy tov�bb, a feladatmegold�sokat k�zrefog� k�t-k�t nyit� �s z�r� prompt sor ism�tl�dik �rtelemszer�en, a task tag n attrib�tum�nak �rt�ke az adott feladat sz�ma.

-- az adatmanipul�ci� feladatai el�tt sz�ks�ges kiadni
set feedback on

prompt <task n="4.1">
prompt <![CDATA[  

-- 4.1-es feladat megold�sa ide

prompt ]]>
prompt </task>
prompt <task n="4.2">
prompt <![CDATA[

-- 4.2-es feladat megold�sa ide

prompt ]]>
prompt </task>
prompt <task n="4.3">
prompt <![CDATA[

-- 4.3-as feladat megold�sa ide

prompt ]]>
prompt </task>

-- az adatmanipul�ci� feladatai ut�n sz�ks�ges kiadni
set feedback off

prompt <task n="5.1">
prompt <![CDATA[ 

-- 5.1-es feladat megold�sa ide

prompt ]]>
prompt </task>
prompt </tasks>
