set echo off
set verify off
alter session set NLS_DATE_FORMAT='YYYY-MM-DD';
set serveroutput on
set feedback off

prompt <tasks>

prompt <task n="1.1">
prompt <![CDATA[

prompt ]]>
prompt </task>

prompt <task n="2.1">
prompt <![CDATA[
SELECT * FROM airplane;
prompt ]]>
prompt </task>

prompt <task n="2.2">
prompt <![CDATA[
SELECT registration, model, range FROM airplane
WHERE propeller = 1;
prompt ]]>
prompt </task>

prompt <task n="2.3">
prompt <![CDATA[
SELECT flight_number, departs_from, arrives_to, pilot FROM flight, schedule
WHERE schedule.flight_id = flight.flight_id AND
flight.departs_at >= 1200
ORDER BY departs_at ASC, pilot ASC;
prompt ]]>
prompt </task>

prompt <task n="2.4">
prompt <![CDATA[
SELECT DISTINCT registration, pilot, notes FROM airplane, schedule
WHERE schedule.airplane_id = airplane.airplane_id
AND airplane.notes IS NOT NULL
ORDER BY airplane.registration ASC, schedule.pilot ASC;
prompt ]]>
prompt </task>

prompt <task n="2.5">
prompt <![CDATA[
SELECT DISTINCT registration, model, maintenance, pilot FROM airplane, schedule
WHERE schedule.airplane_id(+) = airplane.airplane_id
ORDER BY airplane.registration ASC, airplane.maintenance DESC;
prompt ]]>
prompt </task>

prompt <task n="2.6">
prompt <![CDATA[
SELECT registration, model, COUNT (DISTINCT pilot) as pilots FROM airplane, schedule
WHERE schedule.airplane_id(+) = airplane.airplane_id
GROUP BY airplane.registration, airplane.model
ORDER BY airplane.registration ASC;
prompt ]]>
prompt </task>

prompt <task n="2.7">
prompt <![CDATA[
SELECT registration, model, COUNT (DISTINCT pilot) as pilots FROM airplane, schedule
WHERE schedule.airplane_id(+) = airplane.airplane_id
GROUP BY airplane.registration, airplane.model
HAVING COUNT (DISTINCT pilot) <= 2
ORDER BY airplane.registration ASC;
prompt ]]>
prompt </task>

prompt <task n="2.8">
prompt <![CDATA[
SELECT registration, model, pilot FROM airplane, schedule
WHERE schedule.airplane_id(+) = airplane.airplane_id AND
    schedule.days >= 64
UNION
SELECT registration, model, NULL FROM airplane, schedule
WHERE schedule.airplane_id(+) = airplane.airplane_id
ORDER BY registration ASC;
prompt ]]>
prompt </task>
prompt </tasks>