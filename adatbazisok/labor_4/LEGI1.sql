DROP TABLE schedule    CASCADE CONSTRAINTS PURGE;
DROP TABLE airplane      CASCADE CONSTRAINTS PURGE;
DROP TABLE flight    CASCADE CONSTRAINTS PURGE;
DROP SEQUENCE schedule_seq;
DROP SEQUENCE flight_seq;
DROP SEQUENCE airplane_seq;
--
--
------------------------------------------------------------------------------
------------------------------------------------------------------------------
CREATE SEQUENCE schedule_seq MINVALUE 1 MAXVALUE 9999999999 NOCYCLE;
CREATE SEQUENCE flight_seq   MINVALUE 1 MAXVALUE 9999999    NOCYCLE;
CREATE SEQUENCE airplane_seq MINVALUE 1 MAXVALUE 9999999    NOCYCLE;
-------------------------------------------------------------------

CREATE TABLE airplane
(
    airplane_id   NUMBER(7,0)         PRIMARY KEY,
    registration  NVARCHAR2(7)        NOT NULL UNIQUE,
    model         NVARCHAR2(7)        NOT NULL,
    maintenance   DATE                NOT NULL,
    repair        DATE                NOT NULL,
    propeller     NUMBER(1,0)         DEFAULT 1 NOT NULL,
    seats         NUMBER(3,0)         DEFAULT 320 NOT NULL,
    range         NUMBER(7,2)         NOT NULL,
    notes         NVARCHAR2(50),
--
    CONSTRAINT "airplane_maintenance_limit"  CHECK (repair >= TO_DATE('2005-01-01', 'YYYY-MM-DD')),
    CONSTRAINT "airplane_seats_limit"        CHECK (seats BETWEEN 70 AND 856),
    CONSTRAINT "airplane_maintained_since"   CHECK (maintenance >= TO_DATE('1985-01-01', 'YYYY-MM-DD')),
    CONSTRAINT "airplane_range"	             CHECK (range BETWEEN 300 AND 20000),
    CONSTRAINT "airplane_range_propeller"    CHECK (propeller = 0 OR range <= 2000), -- propeller planes have less than 2000 km range
    CONSTRAINT "airplane_propeller"          CHECK (propeller in (0, 1))
);
comment on column airplane.propeller is 'Values 0 and 1 encode boolean values false and true, respectively.';
--
--
------------------------------------------------------------------------------  Jaratok
CREATE TABLE flight
(
    flight_id      NUMBER(7,0)         PRIMARY KEY,
    flight_number  NVARCHAR2(6)        NOT NULL UNIQUE,
    departs_from   NCHAR(3)            NOT NULL,
    arrives_to     NCHAR(3)            NOT NULL,
    departs_at     NUMBER(4,0)         NOT NULL,
    arrives_at     NUMBER(4,0)         NOT NULL,
--
    CONSTRAINT "flight_departure_arrival_limit"  CHECK (departs_at < arrives_at),
    CONSTRAINT "flight_departure_limit"          CHECK (departs_at BETWEEN 0 AND 2359),
    CONSTRAINT "flight_arrival_limit"            CHECK (arrives_at BETWEEN 0 AND 2359),
    CONSTRAINT "flight_departure_minutes"        CHECK (ABS(MOD(departs_at, 100)) < 60),
    CONSTRAINT "flight_arrival_minutes"          CHECK (ABS(MOD(arrives_at, 100)) < 60)
);
--
--
------------------------------------------------------------------------------  Beosztasok
CREATE TABLE schedule
(
    id           NUMBER(10,0)  PRIMARY KEY,
    airplane_id  NUMBER(7,0)   REFERENCES airplane  ON DELETE CASCADE NOT NULL,
    flight_id    NUMBER(7,0)   REFERENCES flight    ON DELETE CASCADE NOT NULL,
    days         NUMBER(3,0)   NOT NULL,
    pilot        NVARCHAR2(256),
--
    CONSTRAINT "days_limit" CHECK (days BETWEEN 1 AND 127)
);
--
--
--
COMMIT;
--
--
------------------------------------------------------------------------------  Alap tesztadatok
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LID', 'B747400', ADD_MONTHS( SYSDATE,  -1  ), ADD_MONTHS( SYSDATE, -6    ), 0, 300, 14205,  'obsolete video setup'     );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LJI', 'B747300', ADD_MONTHS( SYSDATE,  -1  ), ADD_MONTHS( SYSDATE, -6    ), 0, 300, 12400,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LSP', 'MD11',    ADD_MONTHS( SYSDATE,  -2  ), ADD_MONTHS( SYSDATE, -5    ), 0, 240, 12655,  'new air conditioning'     );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LAK', 'B767',    ADD_MONTHS( SYSDATE,  -2  ), ADD_MONTHS( SYSDATE, -5    ), 0, 210, 7300,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LCS', 'B767',    ADD_MONTHS( SYSDATE,  -3  ), ADD_MONTHS( SYSDATE, -4    ), 0, 210, 12200,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LKL', 'B737900', ADD_MONTHS( SYSDATE,  -3  ), ADD_MONTHS( SYSDATE, -4    ), 0,180, 5600,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LFV', 'B737300', ADD_MONTHS( SYSDATE, -0.4 ), ADD_MONTHS( SYSDATE, -3    ), 0, 110, 4300,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LLR', 'Fok70',   ADD_MONTHS( SYSDATE, -1.2 ), ADD_MONTHS( SYSDATE, -3    ), 0, 80,  3410,  'new flaps'                );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LZZ', 'B747300', ADD_MONTHS( SYSDATE, -1.1 ), ADD_MONTHS( SYSDATE, -2    ), 0, 300, 12400,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LZS', 'MD11',    ADD_MONTHS(SYSDATE, -0.8 ), ADD_MONTHS(SYSDATE, -2     ), 0, 240, 12655,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LJT', 'B767',    ADD_MONTHS(SYSDATE, -0.8 ), ADD_MONTHS(SYSDATE, -1     ), 0, 210, 11065,  'flaps should be replaced' );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LCY', 'B737900', ADD_MONTHS(SYSDATE,  -1  ), ADD_MONTHS(SYSDATE, -1     ), 0, 180, 5600,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LAS', 'B737300', ADD_MONTHS(SYSDATE,  -1  ), ADD_MONTHS(SYSDATE, -9     ), 0, 110, 4300,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LCV', 'Fok70',   ADD_MONTHS(SYSDATE,  -1  ), ADD_MONTHS(SYSDATE, -9     ), 0, 80,  3410,  'old engine'               );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LCA', 'B747300', ADD_MONTHS(SYSDATE,  -2  ), ADD_MONTHS(SYSDATE, -9     ), 0, 300, 12400,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LOV', 'B747400', ADD_MONTHS(SYSDATE,  -2  ), ADD_MONTHS(SYSDATE, -9     ), 0, 250, 14205,  'expanded BUSINESS class'  );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LBL', 'B747400', ADD_MONTHS(SYSDATE, -0.1 ), ADD_MONTHS(SYSDATE, -5.8   ), 0, 300, 14205,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LHN', 'B767',    ADD_MONTHS(SYSDATE, -0.2 ), ADD_MONTHS(SYSDATE, -5.9   ), 0, 210, 6025,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LJP', 'B737900', ADD_MONTHS(SYSDATE, -0.3 ), ADD_MONTHS(SYSDATE, -6.0   ), 0, 180, 5600,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LGJ', 'B737300', ADD_MONTHS(SYSDATE, -0.4 ), ADD_MONTHS(SYSDATE, -6.1   ), 0, 110, 4300,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LZE', 'B747400', ADD_MONTHS(SYSDATE,  -3  ), ADD_MONTHS(SYSDATE, -7     ), 0, 300, 14205,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LWR', 'B747400', ADD_MONTHS(SYSDATE,  -3  ), ADD_MONTHS(SYSDATE, -7     ), 0, 300, 14205,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LIU', 'B767',    ADD_MONTHS(SYSDATE,  -4  ), ADD_MONTHS(SYSDATE, -8     ), 0, 210, 10425,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LZL', 'MD11',    ADD_MONTHS(SYSDATE,  -13 ), ADD_MONTHS(SYSDATE, -15    ), 0, 240, 12655,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LZJ', 'B737900', ADD_MONTHS(SYSDATE,  -5  ), ADD_MONTHS(SYSDATE, -5     ), 0, 180, 5600,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LTD', 'B737300', ADD_MONTHS(SYSDATE,   1  ), ADD_MONTHS(SYSDATE, -5     ), 0, 110, 4300,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LTB', 'B737300', ADD_MONTHS(SYSDATE,  -6  ), ADD_MONTHS(SYSDATE, -9     ), 0, 110, 4300,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LOM', 'Fok70',   ADD_MONTHS(SYSDATE,  -6  ), ADD_MONTHS(SYSDATE, -11    ), 0, 90,  3410,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LJB', 'Fok70',   ADD_MONTHS(SYSDATE,  -7  ), ADD_MONTHS(SYSDATE, -11    ), 0, 90,  3410,  'old engine'               );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LSR', 'CRJ100',  ADD_MONTHS(SYSDATE,  -1  ), ADD_MONTHS(SYSDATE, -5     ), 1, 70,  2000,  'short range'              );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LDC', 'CRJ100',  ADD_MONTHS(SYSDATE,  -7  ), ADD_MONTHS(SYSDATE, -8     ), 0, 70,  2000,  'short range'              );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LMA', 'A320',    ADD_MONTHS(SYSDATE, -0.3 ), ADD_MONTHS(SYSDATE, -1.5   ), 0, 145, 6100,  NULL                       );
INSERT INTO airplane (airplane_id, registration, model, maintenance, repair, propeller, seats, range, notes) VALUES (airplane_seq.nextval, 'HA-LZN', 'A320',    ADD_MONTHS(SYSDATE,  -2  ), ADD_MONTHS(SYSDATE, -4     ), 0, 145, 6100,  NULL                       );


INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7958', 'BUD', 'HEL',  8 *100+ 10,  10 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF9037', 'BUD', 'HEL', 17 *100+ 20,  19 *100+ 45);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3780', 'HEL', 'BUD', 11 *100+ 10,  13 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF1591', 'BUD', 'HEL',  8 *100+ 10,  10 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4290', 'BUD', 'CDG',  6 *100+ 40,   9 *100+ 10);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5035', 'CDG', 'BUD', 12 *100+ 35,  15 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3608', 'AMS', 'CDG',  7 *100+ 10,   8 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5176', 'CDG', 'AMS',  9 *100+ 15,  10 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7333', 'AMS', 'NRT',  6 *100+ 30,  18 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF1977', 'NRT', 'AMS',  8 *100+ 10,  20 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF2694', 'BUD', 'OSL', 21 *100+ 10,  23 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4846', 'BUD', 'OSL',  9 *100+ 40,  12 *100+ 05);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7510', 'OSL', 'HEL', 13 *100+ 10,  13 *100+ 55);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4639', 'NRT', 'HEL',  8 *100+ 10,  19 *100+ 55);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3345', 'HEL', 'NRT',  7 *100+ 00,  18 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5079', 'BUD', 'TXL', 11 *100+ 50,  13 *100+ 25);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5534', 'TXL', 'LON', 15 *100+ 50,  18 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF1893', 'LON', 'BUD', 19 *100+ 10,  22 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5500', 'LON', 'BUD', 19 *100+ 10,  22 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF9852', 'BGO', 'OSL', 17 *100+ 10,  18 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF2414', 'BGO', 'HEL',  6 *100+ 10,   8 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF9743', 'HEL', 'BGO',  9 *100+ 55,  11 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4542', 'BUD', 'FRA',  8 *100+ 10,  10 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3050', 'DEL', 'FRA', 11 *100+ 10,  22 *100+ 40);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4210', 'DEL', 'FRA',  9 *100+ 55,  21 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF0560', 'FRA', 'BKK',  8 *100+ 10,  22 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF0010', 'DEL', 'BKK', 15 *100+ 10,  20 *100+ 05);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF2899', 'CDG', 'IST', 12 *100+ 10,  15 *100+ 15);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF6683', 'ist', 'CDG', 17 *100+ 30,  20 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF9936', 'BUD', 'IST',  5 *100+ 30,   7 *100+ 55);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7226', 'IST', 'BUD',  9 *100+ 10,  11 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF0587', 'BUD', 'IST', 13 *100+ 30,  16 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF1505', 'IST', 'BUD', 18 *100+ 10,  20 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4682', 'BUD', 'NYC',  8 *100+ 10,  19 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7349', 'NYC', 'BUD',  8 *100+ 10,  19 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF0350', 'NYC', 'AMS',  6 *100+ 35,  18 *100+ 05);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF2121', 'AMS', 'NYC', 11 *100+ 10,  21 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5632', 'NYC', 'AMS', 12 *100+ 40,  23 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5798', 'AMS', 'NYC', 13 *100+ 15,  23 *100+ 55);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4369', 'BUD', 'PRG',  8 *100+ 15,   9 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5514', 'BUD', 'PRG', 18 *100+ 15,  19 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3796', 'PRG', 'BUD', 12 *100+ 40,  14 *100+ 10);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7355', 'PRG', 'BUD', 21 *100+ 15,  22 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3346', 'PRG', 'OSL', 11 *100+ 15,  13 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF6314', 'OSL', 'PRG', 15 *100+ 00,  17 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF4768', 'PRG', 'NYC',  9 *100+ 40,  19 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF1677', 'NYC', 'PRG', 10 *100+ 00,  20 *100+ 15);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF8407', 'NYC', 'PRG', 10 *100+ 00,  20 *100+ 15);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7607', 'PRG', 'CDG', 15 *100+ 30,  17 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5041', 'CDG', 'TXL', 19 *100+ 00,  20 *100+ 25);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF5312', 'TXL', 'NRT',  7 *100+ 15,  19 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF9569', 'NRT', 'LON', 10 *100+ 10,  23 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF0361', 'LON', 'IST', 12 *100+ 00,  15 *100+ 00);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7584', 'IST', 'BUD', 17 *100+ 15,  19 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF9215', 'NYC', 'NRT',  6 *100+ 10,  22 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF8966', 'BKK', 'SYD',  6 *100+ 10,  22 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7012', 'SYD', 'MOW',  6 *100+ 10,  22 *100+ 35);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3373', 'BUD', 'ROM',  7 *100+ 15,   9 *100+ 05);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF3257', 'ROM', 'BOS',  11*100+ 45,  20 *100+ 30);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF7606', 'BOS', 'ROM',  12*100+ 00,  19 *100+ 55);
INSERT INTO flight (flight_id, flight_number, departs_from, arrives_to, departs_at, arrives_at) VALUES (flight_seq.nextval, 'BF6577', 'ROM', 'BUD',  21*100+ 00,  22 *100+ 55);

INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Kovacs Istvan'       FROM airplane, flight WHERE airplane.registration = 'HA-LKL' AND flight.flight_number = 'BF7958';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   80,  'Kovacs Zsigmond'     FROM airplane, flight WHERE airplane.registration = 'HA-LSP' AND flight.flight_number = 'BF7958';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   32,  'Nagy Adon'           FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF9037';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    4,  'Nagy Pal'            FROM airplane, flight WHERE airplane.registration = 'HA-LSP' AND flight.flight_number = 'BF1591';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Kovacs Zsigmond'     FROM airplane, flight WHERE airplane.registration = 'HA-LSP' AND flight.flight_number = 'BF1591';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   90,  'Nagy Pal'            FROM airplane, flight WHERE airplane.registration = 'HA-LSP' AND flight.flight_number = 'BF3780';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Kovacs Istvan'       FROM airplane, flight WHERE airplane.registration = 'HA-LKL' AND flight.flight_number = 'BF3780';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    1,  'Kovacs Zsigmond'     FROM airplane, flight WHERE airplane.registration = 'HA-LKL' AND flight.flight_number = 'BF3780';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   32,  'Nagy Adon'           FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF3780';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Eszterhazi Marton'   FROM airplane, flight WHERE airplane.registration = 'HA-LHN' AND flight.flight_number = 'BF4290';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   31,  'Feher Gyula'         FROM airplane, flight WHERE airplane.registration = 'HA-LHN' AND flight.flight_number = 'BF5035';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Eszterhazi Marton'   FROM airplane, flight WHERE airplane.registration = 'HA-LHN' AND flight.flight_number = 'BF5035';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   85,  'Miklos Miklos'       FROM airplane, flight WHERE airplane.registration = 'HA-LJP' AND flight.flight_number = 'BF3608';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   42,  'Feher Gyula'         FROM airplane, flight WHERE airplane.registration = 'HA-LJP' AND flight.flight_number = 'BF3608';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Gyorgy Elek'         FROM airplane, flight WHERE airplane.registration = 'HA-LJP' AND flight.flight_number = 'BF5176';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   84,  'Mezga Geza'          FROM airplane, flight WHERE airplane.registration = 'HA-LZZ' AND flight.flight_number = 'BF7333';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   10,  'Szaraz Emese'        FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF7333';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   33,  'Matuska Szilveszter' FROM airplane, flight WHERE airplane.registration = 'HA-LJT' AND flight.flight_number = 'BF7333';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   33,  'Matuska Szilveszter' FROM airplane, flight WHERE airplane.registration = 'HA-LJT' AND flight.flight_number = 'BF1977';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   41,  'Mezga Geza'          FROM airplane, flight WHERE airplane.registration = 'HA-LZZ' AND flight.flight_number = 'BF1977';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   20,  'Szaraz Emese'        FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF1977';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   32,  'Feher Nandor'        FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF2694';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    4,  'Kekesi Bela'         FROM airplane, flight WHERE airplane.registration = 'HA-LZS' AND flight.flight_number = 'BF2694';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   21,  'Feher Nandor'        FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF4846';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   21,  'Feher Nandor'        FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF7510';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   98,  'Peter Lajos'         FROM airplane, flight WHERE airplane.registration = 'HA-LZS' AND flight.flight_number = 'BF7510';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Feher Nandor'        FROM airplane, flight WHERE airplane.registration = 'HA-LSP' AND flight.flight_number = 'BF7510';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    1,  'Egri Gabor'          FROM airplane, flight WHERE airplane.registration = 'HA-LID' AND flight.flight_number = 'BF4639';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Nagy Adon'           FROM airplane, flight WHERE airplane.registration = 'HA-LJI' AND flight.flight_number = 'BF4639';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    4,  'Gabor Aron'          FROM airplane, flight WHERE airplane.registration = 'HA-LAK' AND flight.flight_number = 'BF4639';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Kristof Attila'      FROM airplane, flight WHERE airplane.registration = 'HA-LBL' AND flight.flight_number = 'BF4639';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   16,  'Egri Gabor'          FROM airplane, flight WHERE airplane.registration = 'HA-LID' AND flight.flight_number = 'BF4639';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   32,  'Szaraz Emese'        FROM airplane, flight WHERE airplane.registration = 'HA-LJI' AND flight.flight_number = 'BF4639';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Egri Gabor'          FROM airplane, flight WHERE airplane.registration = 'HA-LID' AND flight.flight_number = 'BF3345';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    4,  'Nagy Adon'           FROM airplane, flight WHERE airplane.registration = 'HA-LJI' AND flight.flight_number = 'BF3345';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Gabor Aron'          FROM airplane, flight WHERE airplane.registration = 'HA-LAK' AND flight.flight_number = 'BF3345';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   16,  'Kristof Attila'      FROM airplane, flight WHERE airplane.registration = 'HA-LBL' AND flight.flight_number = 'BF3345';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   32,  'Egri Gabor'          FROM airplane, flight WHERE airplane.registration = 'HA-LID' AND flight.flight_number = 'BF3345';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   64,  'Szaraz Emese'        FROM airplane, flight WHERE airplane.registration = 'HA-LJI' AND flight.flight_number = 'BF3345';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   15,  'Gabor Aron'          FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF5079';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Gabor Aron'          FROM airplane, flight WHERE airplane.registration = 'HA-LKL' AND flight.flight_number = 'BF5079';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   15,  'Antal Janos'         FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF5534';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Gabor Aron'          FROM airplane, flight WHERE airplane.registration = 'HA-LKL' AND flight.flight_number = 'BF5534';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   15,  'Antal Janos'         FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF1893';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Gabor Aron'          FROM airplane, flight WHERE airplane.registration = 'HA-LKL' AND flight.flight_number = 'BF1893';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   31,  'Szep Ilona'          FROM airplane, flight WHERE airplane.registration = 'HA-LLR' AND flight.flight_number = 'BF5500';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Szep Ilona'          FROM airplane, flight WHERE airplane.registration = 'HA-LLR' AND flight.flight_number = 'BF5500';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Antal Janos'         FROM airplane, flight WHERE airplane.registration = 'HA-LLR' AND flight.flight_number = 'BF9852';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   31,  'Szep Ilona'          FROM airplane, flight WHERE airplane.registration = 'HA-LLR' AND flight.flight_number = 'BF9852';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Bem Joszef'          FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF2414';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   68,  'Kovacs Istvan'       FROM airplane, flight WHERE airplane.registration = 'HA-LGJ' AND flight.flight_number = 'BF2414';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Bem Joszef'          FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF9743';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   68,  'Benedek Mate'        FROM airplane, flight WHERE airplane.registration = 'HA-LGJ' AND flight.flight_number = 'BF9743';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Szep Ilona'          FROM airplane, flight WHERE airplane.registration = 'HA-LAS' AND flight.flight_number = 'BF4542';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   42,  'Sandor Matyas'       FROM airplane, flight WHERE airplane.registration = 'HA-LFV' AND flight.flight_number = 'BF3050';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   16,  'Benedek Mate'        FROM airplane, flight WHERE airplane.registration = 'HA-LFV' AND flight.flight_number = 'BF3050';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    1,  'Nagy Pal'            FROM airplane, flight WHERE airplane.registration = 'HA-LFV' AND flight.flight_number = 'BF4210';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   64,  'Nagy Adon'           FROM airplane, flight WHERE airplane.registration = 'HA-LID' AND flight.flight_number = 'BF4210';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Hunyadi Matyas'      FROM airplane, flight WHERE airplane.registration = 'HA-LOV' AND flight.flight_number = 'BF0560';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Szep Ilona'          FROM airplane, flight WHERE airplane.registration = 'HA-LAK' AND flight.flight_number = 'BF0010';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    3,  'Bem Joszef'          FROM airplane, flight WHERE airplane.registration = 'HA-LGJ' AND flight.flight_number = 'BF0010';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Balogh Adon'         FROM airplane, flight WHERE airplane.registration = 'HA-LCY' AND flight.flight_number = 'BF2899';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Balogh Adon'         FROM airplane, flight WHERE airplane.registration = 'HA-LCY' AND flight.flight_number = 'BF6683';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Kovacs Lajos'        FROM airplane, flight WHERE airplane.registration = 'HA-LZL' AND flight.flight_number = 'BF9936';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Mezga Geza'          FROM airplane, flight WHERE airplane.registration = 'HA-LZL' AND flight.flight_number = 'BF9936';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Kovacs Lajos'        FROM airplane, flight WHERE airplane.registration = 'HA-LZL' AND flight.flight_number = 'BF7226';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Piros Farkas'        FROM airplane, flight WHERE airplane.registration = 'HA-LZL' AND flight.flight_number = 'BF7226';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Kovacs Lajos'        FROM airplane, flight WHERE airplane.registration = 'HA-LZL' AND flight.flight_number = 'BF0587';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  112,  'Kovacs Lajos'        FROM airplane, flight WHERE airplane.registration = 'HA-LZL' AND flight.flight_number = 'BF1505';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   21,  'Egri Ede'            FROM airplane, flight WHERE airplane.registration = 'HA-LZE' AND flight.flight_number = 'BF4682';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   42,  'Kardos Mihaly'       FROM airplane, flight WHERE airplane.registration = 'HA-LZE' AND flight.flight_number = 'BF4682';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   42,  'Mezga Aladar'        FROM airplane, flight WHERE airplane.registration = 'HA-LOM' AND flight.flight_number = 'BF7349';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   84,  'Kardos Mark'         FROM airplane, flight WHERE airplane.registration = 'HA-LOM' AND flight.flight_number = 'BF7349';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    5,  'Kovacs Lajos'        FROM airplane, flight WHERE airplane.registration = 'HA-LBL' AND flight.flight_number = 'BF0350';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   10,  'Lelkes Janos'        FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF0350';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   10,  'Kovacs Lajos'        FROM airplane, flight WHERE airplane.registration = 'HA-LBL' AND flight.flight_number = 'BF2121';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   20,  'Lelkes Janos'        FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF2121';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   32,  'Lelkes Janos'        FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF5632';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   64,  'Lelketlen Nandor'    FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF5798';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  117,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF4369';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Kardigan Alajos'     FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF4369';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF4369';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF5514';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   11,  'Kardigan Alajos'     FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF5514';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  117,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF3796';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    2,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF3796';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    8,  'Kardigan Alajos'     FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF3796';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   11,  'Kardigan Alajos'     FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF7355';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LJB' AND flight.flight_number = 'BF7355';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   21,  'Sandor Matyas'       FROM airplane, flight WHERE airplane.registration = 'HA-LZJ' AND flight.flight_number = 'BF3346';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   64,  'Dolgos Panna'        FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF3346';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   21,  'Sandor Matyas'       FROM airplane, flight WHERE airplane.registration = 'HA-LZJ' AND flight.flight_number = 'BF6314';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   64,  'Egri Ede'            FROM airplane, flight WHERE airplane.registration = 'HA-LCV' AND flight.flight_number = 'BF6314';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Alma Istvanne'       FROM airplane, flight WHERE airplane.registration = 'HA-LWR' AND flight.flight_number = 'BF4768';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Jakupcsek Anita'     FROM airplane, flight WHERE airplane.registration = 'HA-LWR' AND flight.flight_number = 'BF1677';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   97,  'Cseresznye Virag'    FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF8407';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   97,  'Cseresznye Virag'    FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF8407';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   97,  'Eszterhazi Marton'   FROM airplane, flight WHERE airplane.registration = 'HA-LCA' AND flight.flight_number = 'BF7607';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   42,  'Kistarcsai Adon'     FROM airplane, flight WHERE airplane.registration = 'HA-LSR' AND flight.flight_number = 'BF7607';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   68,  'Orosz Tamara'        FROM airplane, flight WHERE airplane.registration = 'HA-LIU' AND flight.flight_number = 'BF5041';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   96,  'Orosz Tamara'        FROM airplane, flight WHERE airplane.registration = 'HA-LIU' AND flight.flight_number = 'BF5312';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Barna Gabor'         FROM airplane, flight WHERE airplane.registration = 'HA-LTD' AND flight.flight_number = 'BF9569';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Ivan Lajos'          FROM airplane, flight WHERE airplane.registration = 'HA-LTD' AND flight.flight_number = 'BF0361';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   42,  'Kek Elemer'          FROM airplane, flight WHERE airplane.registration = 'HA-LTB' AND flight.flight_number = 'BF7584';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,    1,  'Kardos Mihaly'       FROM airplane, flight WHERE airplane.registration = 'HA-LJI' AND flight.flight_number = 'BF7584';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   64,  'Kekesi Tibor'        FROM airplane, flight WHERE airplane.registration = 'HA-LFV' AND flight.flight_number = 'BF7584';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,   16,  'Kekesi Bela'         FROM airplane, flight WHERE airplane.registration = 'HA-LJI' AND flight.flight_number = 'BF7584';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Egri Ede'            FROM airplane, flight WHERE airplane.registration = 'HA-LMA' AND flight.flight_number = 'BF3373';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Benedek Elek Andras' FROM airplane, flight WHERE airplane.registration = 'HA-LAK' AND flight.flight_number = 'BF3257';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Benedek Elek Andras' FROM airplane, flight WHERE airplane.registration = 'HA-LAK' AND flight.flight_number = 'BF3257';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Toth Peter'          FROM airplane, flight WHERE airplane.registration = 'HA-LTB' AND flight.flight_number = 'BF7606';
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot) SELECT schedule_seq.nextval, airplane.airplane_id, flight.flight_id,  127,  'Varna Zsolt'         FROM airplane, flight WHERE airplane.registration = 'HA-LCS' AND flight.flight_number = 'BF6577';




--
--
COMMIT;
