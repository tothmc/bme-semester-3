set echo off
set verify off
alter session set NLS_DATE_FORMAT='YYYY-MM-DD';
set serveroutput on
set feedback off

prompt <tasks>

prompt <task n="3.1">
prompt <![CDATA[
INSERT INTO airplane(airplane_id, model, propeller, registration, seats, range, maintenance, repair) 
VALUES (airplane_seq.NextVal, 'B77781', 0, 'HA-LOC', 561, 16000, SYSDATE, SYSDATE);
prompt ]]>
prompt </task>

prompt <task n="3.2">
prompt <![CDATA[
UPDATE airplane
SET seats = 280, notes = 'VIP seats with more legroom in the front'
WHERE model = 'B747400' AND registration = 'HA-LID';
prompt ]]>
prompt </task>

prompt <task n="3.3">
prompt <![CDATA[
UPDATE airplane
SET seats = seats*0.9
WHERE model LIKE 'B%' AND range BETWEEN 5000 AND 8000;
prompt ]]>
prompt </task>

prompt <task n="3.4">
prompt <![CDATA[
UPDATE airplane
SET airplane.notes = 'APU behaves erratically'
WHERE airplane.airplane_id IN (SELECT airplane.airplane_id FROM airplane, schedule
                                WHERE airplane.airplane_id = schedule.airplane_id AND schedule.pilot = 'Sandor Matyas' AND airplane.model = 'B737300');
prompt ]]>
prompt </task>

prompt <task n="3.5">
prompt <![CDATA[
DELETE flight
WHERE  departs_at <= 700;
prompt ]]>
prompt </task>

prompt <task n="3.6">
prompt <![CDATA[
INSERT INTO schedule (id, airplane_id, flight_id, days, pilot)
VALUES(  schedule_seq.NextVal, 
        (SELECT airplane_id FROM airplane WHERE registration = 'HA-LOC'),
        (SELECT flight_id FROM flight WHERE flight_number = 'BF1505'),
        64,
        'Helen Antal');
prompt ]]>
prompt </task>
prompt </tasks>